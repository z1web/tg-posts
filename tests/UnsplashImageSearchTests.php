<?php

use App\Service\UnsplashImageSearchService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class UnsplashImageSearchTests extends KernelTestCase
{
    private ?object $client;

    protected function setUp(): void
    {
        self::bootKernel();
        $container = static::getContainer();
        $this->client = $container->get(UnsplashImageSearchService::class);
    }

    public function testSearch(): void
    {
        $result = $this->client->request('counter strike');
        dump($result);
        $this->assertIsArray($result);
    }
}