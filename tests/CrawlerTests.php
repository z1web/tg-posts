<?php

use PHPUnit\Framework\TestCase;
use App\Service\OpenAi\OpenAiGenerateMetatagsRequest;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class CrawlerTests extends KernelTestCase
{
    public function testCrawler()
    {
        $html = '\n
        \n
        <!DOCTYPE html>\n
        <html lang="en">\n
        <head>\n
        \t<meta charset="UTF-8">\n
        \t<title>Новости криптовалюты</title>\n
        </head>\n
        <body>\n
        \t<h1>Новости криптовалюты</h1>\n
        \t<p>Криптовалюты продолжают привлекать внимание инвесторов и общественности, несмотря на нестабильность их курса. Ниже представлены некоторые из последних новостей в этой области.</p>\n
        </body>\n
        </html>';

        $crawler = new \Symfony\Component\DomCrawler\Crawler();
        $crawler->addHtmlContent($html);
        $content = $crawler->filter('body')->first()->html();
        dump($content);
    }
}