<?php

use App\Service\OpenAi\OpenAiRequestExecutor;
use App\Service\OpenAi\OpenAiClient;
use App\Service\OpenAi\Requests\OpenAiGenerateThemesRequest;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use App\Service\OpenAi\Requests\OpenAiGenerateMetatagsRequest;
use App\Service\OpenAi\Requests\OpenAiGenerateArticleRequest;
use App\Service\OpenAi\Requests\OpenAiGenerateAppendArticleRequest;

class OpenAiTests extends KernelTestCase
{
    private ?object $client;
    private ?object $executor;

    protected function setUp(): void
    {
        self::bootKernel();
        $container = static::getContainer();
        $this->client = $container->get(OpenAiClient::class);
        $this->executor = $container->get(OpenAiRequestExecutor::class);
    }

    public function testCompletions(): void
    {
        $requestText = 'Составь ключевые слова в формате json по теме : криптовалюта';
        $result = $this->client->request('completions', $requestText);

        dump($result);
        $this->assertIsString($result, 'This is not a string');
    }

    public function testExecuteRequestMetatags(): void
    {
        $request = new OpenAiGenerateMetatagsRequest();
        $request->buildPromtByTheme('counter strike');
        $result = $this->executor->execute($request);

        dump($result);

        $this->assertIsArray($result);
    }

    public function testExecuteRequestMetatagsByUrl(): void
    {
        $request = new OpenAiGenerateMetatagsRequest();
        $request->buildPromtByUrl('http://postio.online/channel/scamsociety/poleznye-neyronki-chital');
        $result = $this->executor->execute($request);

        dump($result);

        $this->assertIsArray($result);
    }

    public function testExecuteRequestArticle(): void
    {
        $request = new OpenAiGenerateArticleRequest();
        $request->buildPromt('криптовалюта');
        dump ($request->getPrompt());
        $result = $this->executor->execute($request);

        dump($result);

        $this->assertIsString($result);
    }

    public function testExecuteRequestAppendArticle(): void
    {
        $request = new OpenAiGenerateAppendArticleRequest();
        $request->setUrl('http://postio.online/channel/scamsociety/poleznye-neyronki-chital');
        $result = $this->executor->execute($request);

        dump($result);

        $this->assertIsString($result);
    }

    public function testPromtBuilder(): void
    {
        $builder = new \App\Service\OpenAi\OpenAiPromtBuilder();

        $promt = $builder
            ->setFormat('json')
            ->setSubject('статью')
            ->setSource('theme', 'крипта')
            ->build();

        dump($promt);
        $this->assertIsString($promt);

        $promt2 = $builder
            ->setFormat('html')
            ->setSubject('ключевые слова')
            ->setSource('url', 'http://ya.ru')
            ->build();

        dump($promt2);
        $this->assertIsString($promt2);
    }

    public function testThemesGeneration(): void
    {
        $request = new OpenAiGenerateThemesRequest();
        $request->buildPromt('как устроится в айти?');
        dump($request->getPrompt());
        $result = $this->executor->execute($request);
        dump($result);
        $this->assertIsArray($result);
    }
}