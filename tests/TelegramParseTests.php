<?php

use App\Entity\ChannelMessages;
use PHPUnit\Framework\TestCase;

final class TelegramParseTests extends TestCase
{
    public function testParseChannel(): void
    {
        $service = new \App\Service\TelegramParseService();
        $data = $service->getChannelMessages('kremgalya');
        $this->assertIsArray($data, 'Is not a JSON');
    }

    public function testParseMetaTags()
    {
        $service = new \App\Service\TextParserService();
        $string = 'string #tag1 sddsjkdskj <strong>#tag22</strong> dsdasd ssd';

        $tags = $service->getTagsFromString($string);
        var_dump($tags);
        $this->assertIsArray($tags, 'Its not a array');
    }
}