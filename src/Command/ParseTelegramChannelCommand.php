<?php

namespace App\Command;

use App\Entity\ChannelMessages;
use App\Service\TelegramParseService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'app:parse-channel',
    description: 'Parse tg data from channel.',
    hidden: false
)]
class ParseTelegramChannelCommand extends Command
{
    protected EntityManagerInterface $entityManager;

    protected static $defaultName = 'app:parse-channel';

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->addArgument('channel-name', InputArgument::OPTIONAL, 'Channel name?');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $channelName = $input->getArgument('channel-name');

        $tgApi = new TelegramParseService();

        foreach ($tgApi->getChannelMessages($channelName, 30) as $num => $messageItem) {
            $output->writeln(['============']);
            $output->writeln(['Start parse ! #' . $num]);
            if (!$messageItem['message']) {
                continue;
            }

            if ($this->entityManager->getRepository(ChannelMessages::class)->findOneBy([
                    'message_id' => $messageItem['id'],
                    'channel_name' => $channelName
                ]) !== null) {
                $output->writeln(['Message ' . $messageItem['id'] . ' exist']);
                continue;
            }

            $output->writeln(['Getting image link']);
            $image = @$tgApi->parseMediaFromMessage($channelName, (array)$messageItem, 'link');

            if (!$image) {
                $output->writeln(['Message ' . $messageItem['id'] . ' dont have image!']);
                continue;
            }

            $imageFileName = $channelName . '_' . $messageItem['id'] . '.' . pathinfo($image, PATHINFO_EXTENSION);

            $output->writeln(['Getting image content']);
            $fileGetResult = file_get_contents($image);
            if (!$fileGetResult) {
                $output->writeln(['File get problem!']);
                return Command::FAILURE;
            }

            $output->writeln(['Saving file process']);
            $fileSaveResult = file_put_contents(
                __DIR__ . '/../../public/images/messages/' . $imageFileName, $fileGetResult
            );
            if (!$fileSaveResult) {
                $output->writeln(['File saving problem!']);
                return Command::FAILURE;
            }

            $message = new ChannelMessages();
            $message->setMessageBody($messageItem['message']);
            $message->setDate($messageItem['date']);
            $message->setChannelName($channelName);
            $message->setMessageId($messageItem['id']);
            $message->setViews($messageItem['views']);
            $message->setImageMain($imageFileName);

            $this->entityManager->persist($message);
            $output->writeln(['Good!']);

        }

        $this->entityManager->flush();

        $output->writeln([
            '============',
            'Done',
            '============',
        ]);

        if (isset($message)) {
            $output->writeln(['Last record id: ' . $message->getId()]);
            return Command::SUCCESS;
        }

        return Command::FAILURE;
    }
}