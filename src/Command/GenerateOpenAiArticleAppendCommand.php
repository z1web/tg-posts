<?php

namespace App\Command;

use App\Entity\ChannelMessages;
use App\Service\OpenAi\Requests\OpenAiGenerateAppendArticleRequest;
use App\Service\OpenAi\OpenAiRequestExecutor;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'app:generate-append-article',
    description: 'SQL Adding tool.',
    hidden: false
)]
class GenerateOpenAiArticleAppendCommand extends Command
{
    protected EntityManagerInterface $entityManager;
    protected OpenAiRequestExecutor $aiRequestExecutor;
    protected static $defaultName = 'app:generate-append-article';

    public function __construct(EntityManagerInterface $entityManager, OpenAiRequestExecutor $aiRequestExecutor)
    {
        $this->entityManager = $entityManager;
        $this->aiRequestExecutor = $aiRequestExecutor;
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->addArgument('message-id', InputArgument::OPTIONAL, 'Id?');
    }


    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $repository = $this->entityManager->getRepository(ChannelMessages::class);
        if ($messageId = $input->getArgument('message-id')) {
            $messages = $repository->findBy(['id' => $messageId]);
        } else {
            $messages = $repository->findBy(array(), array('date' => 'DESC'));
        }

        /**
         * @var $message ChannelMessages
         */
        foreach ($messages as $message) {
            dump($message->getId());
            $url = 'https://postio.online/channel/' . $message->getChannelName() . '/' . $message->getSlug();
            $aiRequest = new OpenAiGenerateAppendArticleRequest();
            $aiRequest->setUrl($url);
            $article = $this->aiRequestExecutor->execute($aiRequest);
            if ($article) {
                $message->setMessageBody($message->getMessageBody() . '<br>' . $article);
            }
            $this->entityManager->flush();
        }

        $output->writeln([
            '============',
            'Done',
            '============',
        ]);

        return Command::SUCCESS;

    }
}