<?php

namespace App\Command;

use App\Entity\ChannelMessages;
use App\Entity\MessageTags;
use App\Service\GoogleSuggestionsService;
use App\Service\OpenAi\Requests\OpenAiGenerateMetatagsRequest;
use App\Service\OpenAi\OpenAiRequestExecutor;
use App\Service\TextParserService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'app:generate-message-tags',
    description: 'SQL Adding tool.',
    hidden: false
)]
class GenerateMessageTagsCommand extends Command
{
    protected EntityManagerInterface $entityManager;
    protected OpenAiRequestExecutor $aiRequestExecutor;
    protected static $defaultName = 'app:generate-message-tags';

    public function __construct(EntityManagerInterface $entityManager, OpenAiRequestExecutor $aiRequestExecutor)
    {
        $this->entityManager = $entityManager;
        $this->aiRequestExecutor = $aiRequestExecutor;
        parent::__construct();
    }

    protected function configure(): void
    {
        // ...
    }


    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $messages = $this->entityManager
            ->getRepository(ChannelMessages::class)
            ->findBy(array(), array('date' => 'DESC'));

        $textParser = new TextParserService();
        $googleSuggestions = new GoogleSuggestionsService();

        /**
         * @var $message ChannelMessages
         */
        foreach ($messages as $message) {
            $aiRequest = new OpenAiGenerateMetatagsRequest();
            $aiRequest->buildPromtByUrl('https://postio.online/channel/' . $message->getChannelName() . '/' . $message->getSlug());
            $aiTags = $this->aiRequestExecutor->execute($aiRequest);

            dump($aiTags);

            foreach ($aiTags as $aiTag) {
                $tagEntity = new MessageTags();
                $tagEntity->setMessageId($message->getId());
                $tagEntity->setType('ai');
                $tagEntity->setValue($aiTag);
                $this->entityManager->persist($tagEntity);
            }

            $this->entityManager->flush();

            $tags = $textParser->getTagsFromString($message->getMessageBody());
            if (!$tags) {
                continue;
            }
            foreach ($tags as $tag) {
                $tagExist = $this->entityManager->getRepository(MessageTags::class)->findOneBy([
                    'message_id' => $message->getId(),
                    'value' => $tag
                ]);

                if ($tagExist !== null) {
                    $output->writeln(['Tag for message '.$message->getId().' exist']);
                    continue;
                }

                $tagEntity = new MessageTags();
                $tagEntity->setMessageId($message->getId());
                $tagEntity->setType('default');
                $tagEntity->setValue($tag);

                $this->entityManager->persist($tagEntity);
                $this->entityManager->flush();

                foreach ($googleSuggestions->getTagsFromGoogle($tag) as $value) {
                    $tagEntityGoogle = new MessageTags();
                    $tagEntityGoogle->setMessageId($message->getId());
                    $tagEntityGoogle->setValue($value);
                    $tagEntityGoogle->setParentId($tagEntity->getId());
                    $tagEntityGoogle->setType('google');

                    $this->entityManager->persist($tagEntityGoogle);
                    $this->entityManager->flush();
                }
            }
        }
        
        $output->writeln([
            '============',
            'Done',
            '============',
        ]);

        return Command::SUCCESS;

    }
}