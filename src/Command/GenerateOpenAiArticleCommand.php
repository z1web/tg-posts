<?php

namespace App\Command;

use App\Entity\ChannelMessages;
use App\Service\OpenAi\Requests\OpenAiGenerateArticleRequest;
use App\Service\OpenAi\OpenAiRequestExecutor;
use App\Service\OpenAi\Requests\OpenAiGenerateParagraphRequest;
use App\Service\OpenAi\Requests\OpenAiGenerateThemesRequest;
use App\Service\UnsplashImageSearchService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'app:generate-article',
    description: 'Generate SEO Article by using ChatGPT',
    hidden: false
)]
class GenerateOpenAiArticleCommand extends Command
{
    protected EntityManagerInterface $entityManager;
    protected OpenAiRequestExecutor $aiRequestExecutor;
    protected UnsplashImageSearchService $imageSearchService;

    protected static $defaultName = 'app:generate-article';

    public function __construct(
        EntityManagerInterface     $entityManager,
        OpenAiRequestExecutor      $aiRequestExecutor,
        UnsplashImageSearchService $imageSearchService
    )
    {
        $this->entityManager = $entityManager;
        $this->aiRequestExecutor = $aiRequestExecutor;
        $this->imageSearchService = $imageSearchService;
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->addArgument('article-theme', InputArgument::OPTIONAL, 'Article theme?');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $articleTheme = $input->getArgument('article-theme');

        $output->writeln([
            '============',
            'Article generation by theme ' . $articleTheme . ' started! ',
            '============',
        ]);

        $articleContent = '';

        /**
         * TODO: Generate title!
         * TODO: Multiple request!
         */

        $aiThemesRequest = new OpenAiGenerateThemesRequest();
        $aiThemesRequest->buildPromt($articleTheme);
        $themes = $this->aiRequestExecutor->execute($aiThemesRequest);

        foreach ($themes as $subTheme) {
            $aiParagraphRequest = new OpenAiGenerateParagraphRequest();
            $aiParagraphRequest->buildPromt($subTheme);
            $subArticleContent = $this->aiRequestExecutor->execute($aiParagraphRequest);
            if ($subArticleContent) {
                $articleContent .= $subArticleContent . '<br>';
            }
        }

        $image = $this->saveImage(
            @$this->imageSearchService->request($articleTheme)['results'][0]['urls']['small'], $output
        );

        $message = new ChannelMessages();
        $message->setMessageBody($articleContent);
        $message->setDate(time());
        $message->setChannelName('default');
        $message->setImageMain($image);

        $this->entityManager->persist($message);
        $this->entityManager->flush();

        $output->writeln(['Good!']);

        return Command::SUCCESS;
    }

    public function saveImage($imageUrl, $output): ?string
    {
        if (!$imageUrl) {
            return null;
        }
        $output->writeln(['Getting image content']);
        if (!$fileGetResult = file_get_contents($imageUrl)) {
            $output->writeln(['File get problem!']);
        }

        $output->writeln(['Saving file process']);
        $randString = substr(str_shuffle("1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ"), -5);
        $imageFileName = 'default' . '_' . $randString . '.' . pathinfo($imageUrl, PATHINFO_EXTENSION);
        if (!file_put_contents(
            __DIR__ . '/../../public/images/messages/' . $imageFileName, $fileGetResult
        )) {
            $output->writeln(['File saving problem!']);
        }

        return $imageFileName;
    }
}