<?php

namespace App\Entity;

use App\Repository\ChannelMessagesRepository;
use Cocur\Slugify\Slugify;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ChannelMessagesRepository::class)]
#[ORM\HasLifecycleCallbacks]
class ChannelMessages
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'integer', nullable: true)]
    private ?int $channel_id;

    #[ORM\Column(type: 'text', nullable: true)]
    private $message_body;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $date;

    #[ORM\Column(type: 'datetime', nullable: true, options: ['default' => 'CURRENT_TIMESTAMP'])]
    private $date_created;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $channel_name;

    #[ORM\Column(type: 'text', nullable: true)]
    private $image_main;

    #[ORM\Column(type: 'integer', nullable: true)]
    private $message_id;

    #[ORM\Column(type: 'string', length: 500)]
    private $slug;

    #[ORM\Column(type: 'text', nullable: true)]
    private $base64_main_image;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $views;

    public function __construct()
    {
        $this->date_created = new \DateTime('now');
    }

    #[ORM\PrePersist]
    #[ORM\PreUpdate]
    public function setSlug()
    {
        $slugify = new Slugify();
        $slugify->activateRuleSet('russian');

        $slugString = strip_tags($this->message_body);
        $slugString = preg_match("/^(.{40,}?)\s+/s", $slugString, $m) ? $m[1] . '' : $slugString;

        $slugString = $slugify->slugify($slugString);

        $this->slug = $slugString;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getChannelId(): ?int
    {
        return $this->channel_id;
    }

    public function setChannelId(?int $channel_id): self
    {
        $this->channel_id = $channel_id;
        return $this;
    }

    public function getMessageBody(): ?string
    {
        return $this->message_body;
    }

    public function setMessageBody(?string $message_body): self
    {
        $this->message_body = $message_body;

        return $this;
    }


    public function getDate(): ?string
    {
        return $this->date;
    }

    public function setDate(?string $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getDateCreated(): ?\DateTimeInterface
    {
        return $this->date_created;
    }

    public function setDateCreated(?\DateTimeInterface $date_created): self
    {
        $this->date_created = $date_created;

        return $this;
    }

    public function getChannelName(): ?string
    {
        return $this->channel_name;
    }

    public function setChannelName(?string $channel_name): self
    {
        $this->channel_name = $channel_name;

        return $this;
    }

    public function getImageMain(): ?string
    {
        return $this->image_main;
    }

    public function setImageMain(?string $image_main): self
    {
        $this->image_main = $image_main;

        return $this;
    }

    public function getMessageId(): ?int
    {
        return $this->message_id;
    }

    public function setMessageId(?int $message_id): self
    {
        $this->message_id = $message_id;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

//    public function setSlug(string $slug): self
//    {
//        $this->slug = $slug;
//
//        return $this;
//    }

public function getBase64MainImage(): ?string
{
    return $this->base64_main_image;
}

public function setBase64MainImage(?string $base64_main_image): self
{
    $this->base64_main_image = $base64_main_image;

    return $this;
}

public function getViews(): ?string
{
    return $this->views;
}

public function setViews(?string $views): self
{
    $this->views = $views;

    return $this;
}

}
