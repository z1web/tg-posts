<?php

namespace App\Entity;

use App\Repository\MessageTagsRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: MessageTagsRepository::class)]
class MessageTags
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'integer')]
    private $message_id;

    #[ORM\Column(type: 'string', length: 800)]
    private $value;

    #[ORM\Column(type: 'string', length: 800)]
    private $type;

    #[ORM\Column(type: 'integer')]
    private $parent_id;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMessageId(): ?int
    {
        return $this->message_id;
    }

    public function setMessageId(int $message_id): self
    {
        $this->message_id = $message_id;

        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $value): self
    {
        $this->type = $value;

        return $this;
    }

    public function getParentId(): ?int
    {
        return $this->parent_id;
    }

    public function setParentId(int $value): self
    {
        $this->parent_id = $value;

        return $this;
    }
}
