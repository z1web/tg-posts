<?php

namespace App\Service;

class TextParserService
{
    public function parseHeaderFromText(string $text): string
    {
        $text = strip_tags(str_replace('<', ' <', $text));
        $SEPARATOR_REPLACE_1 = array(PHP_EOL, '.', '!', '?');
        $SEPARATOR_REPLACE_2 = array('. ', '' . PHP_EOL, '!' . PHP_EOL, '?' . PHP_EOL);
        $EXP  = array_diff(array_map('trim', explode(PHP_EOL, str_replace($SEPARATOR_REPLACE_1, $SEPARATOR_REPLACE_2, $text))), array(''));

        if (isset($EXP[0])) {
            return strip_tags($EXP[0]);
        } elseif (isset($EXP[1])) {
            return strip_tags($EXP[1]);
        } elseif (isset($EXP[2])) {
            return strip_tags($EXP[2]);
        }

        return 'MessageHeader';
    }

    public function getTagsFromString($template): array|string|null
    {
        $tags = [];
        $template = strip_tags($template);
        if (preg_match_all('/"([^"]+)"/', $template, $matches)) {
            for ($i = 0; $i < count($matches[0]); $i++) {
                $str = str_replace('"', '', $matches[0][$i]);
                if (strlen($str) <= 50) {
                    $tags[] = $str;
                }
            }
        }

        if (preg_match_all('/([^«]+)»/', $template, $matches)) {
            for ($i = 0; $i < count($matches[0]); $i++) {
                $str = str_replace('«', '', $matches[0][$i]);
                $str = str_replace('»', '', $str);
                if (strlen($str) <= 50) {
                    $tags[] = $str;
                }
            }
        }

        preg_match_all("/(?<=#)\S*/", $template, $matches);
        if (isset($matches[0])) {
            foreach ($matches[0] as $value) {
                $tags[] = str_replace('#', '', trim($value));
            }
        }

        return $tags;
    }
}