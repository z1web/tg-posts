<?php

namespace App\Service;

use Symfony\Component\HttpClient\Exception\TransportException;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class UnsplashImageSearchService
{
    protected HttpClientInterface $httpClient;
    protected string $url = 'https://api.unsplash.com/search/photos/';
    protected string $apiKey;

    public function __construct(string $apiKey)
    {
        $this->apiKey = $apiKey;
        $this->httpClient = HttpClient::create();
    }

    public function request(string $keyword): array
    {
        $getParams = '?' . http_build_query([
            'client_id' => $this->apiKey, 'query' => $keyword, 'page'  => 1
        ]);

        try {
            $response = $this->httpClient->request('GET', $this->url . $getParams, [
                'headers' => [
                    'Content-Type' => 'application/json',
                ],
                'timeout' => 150
            ]);
        } catch (ClientExceptionInterface|TransportExceptionInterface $e) {
            throw new \RuntimeException($e->getMessage());
        }

        try {
            $result = $response->toArray();
        } catch (TransportExceptionInterface
        | ClientExceptionInterface
        | DecodingExceptionInterface
        | RedirectionExceptionInterface
        | ServerExceptionInterface $e) {
            throw new TransportException($e->getMessage());
        }

        return $result;
    }
}