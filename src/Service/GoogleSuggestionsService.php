<?php

namespace App\Service;

class GoogleSuggestionsService
{
    public function getTagsFromGoogle(string $string): array
    {
        $url = 'https://www.google.com/s?gs_rn=18&gs_ri=psy-ab&cp=7&gs_id=d7&xhr=t&q=' . urlencode($string);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $json = curl_exec($ch);

        $result = json_decode(trim($json), 1);
        $arr = [];

        if (isset($result[1])) {
            foreach ($result[1] as $item) {
                $arr[] = html_entity_decode(@$item[0]);
            }
        }

        return $arr;
    }
}