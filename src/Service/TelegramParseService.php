<?php

namespace App\Service;

use Symfony\Component\DomCrawler\Crawler;

class TelegramParseService
{
    public function getChannelMessages($channelName, $limit = 10): ?array
    {
        $url = 'https://tg.i-c-a.su/json/' . $channelName.'?limit=' . $limit;
        $ch = \curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $data = curl_exec($ch);

        if (!$data) {
            return null;
        }

        $data = json_decode($data,1);

        if (!isset($data['messages'])) {
            return null;
        }

        return $data['messages'];
    }

    public function parseMediaFromMessage(string $channelName, array $message, string $type = 'link'): string
    {
        if (empty($message['media'])) {
            return '';
        }
        $messageUrl = 'https://t.me/' . $channelName . '/' . $message['id'] . '?embed=1';
        $crawler = new Crawler();
        $crawler->addHtmlContent(file_get_contents($messageUrl));
        $images = $crawler
            ->filter('.tgme_widget_message_photo_wrap')
            ->each(function (Crawler $node, $i) {
                return preg_replace('/[\s\S]*background-image:[ ]*url\(["\']*([\s\S]*[^"\'])["\']*\)[\s\S]*/u',
                '$1',$node->attr('style'));
            });

        if (!$images) {
            $images = $crawler
                ->filter('.link_preview_image')
                ->each(function (Crawler $node, $i) {
                    return preg_replace('/[\s\S]*background-image:[ ]*url\(["\']*([\s\S]*[^"\'])["\']*\)[\s\S]*/u',
                    '$1',$node->attr('style'));
                });
        }

        if (!$images) {
            return '';
        }

        if ($type === 'base64') {
            return $this->imageToBase64($images[0]);
        }

        return $images[0];
    }

    public function imageToBase64(string $imagePath): string
    {
        $type = pathinfo($imagePath, PATHINFO_EXTENSION);
        $data = file_get_contents($imagePath);
        return 'data:image/' . $type . ';base64,' . base64_encode($data);
    }
}