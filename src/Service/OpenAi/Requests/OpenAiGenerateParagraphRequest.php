<?php

namespace App\Service\OpenAi\Requests;

use App\Service\OpenAi\OpenAiRequest;
use App\Service\OpenAi\ResponseHandlers\OpenAiHtmlBodyResponseHandler;

class OpenAiGenerateParagraphRequest extends OpenAiRequest
{
    private string $subject = 'абзац для статьи';
    private string $format = 'html';
    private string $sourceType = 'theme';

    public function __construct()
    {
        parent::__construct();
        $this->handler->addHandler(new OpenAiHtmlBodyResponseHandler());
    }

    public function buildPromt(string $articleTheme): void
    {
        $promt = $this->promtBuilder
            ->setSubject($this->subject)
            ->setFormat($this->format)
            ->setSource($this->sourceType, $articleTheme)
            ->build();

        $this->setPrompt($promt);
    }
}
