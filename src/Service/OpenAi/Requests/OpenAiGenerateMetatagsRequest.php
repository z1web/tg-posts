<?php

namespace App\Service\OpenAi\Requests;

use App\Service\OpenAi\OpenAiRequest;
use App\Service\OpenAi\ResponseHandlers\OpenAiArrayToJsonResponseHandler;
use App\Service\OpenAi\ResponseHandlers\OpenAiJsonToArrayResponseHandler;

class OpenAiGenerateMetatagsRequest extends OpenAiRequest
{
    public function __construct()
    {
        parent::__construct();
        $this->handler->addHandler(new OpenAiJsonToArrayResponseHandler());
        #$this->handler->addHandler(new OpenAiArrayToJsonResponseHandler());
    }

    public function buildPromtByTheme(string $tagsTheme): void
    {
        $this->setPrompt(
            $this->promtBuilder
                ->setSubject('список ключевых слов')
                ->setFormat('json')
                ->setSource('theme', $tagsTheme)
                ->build()
        );
    }

    public function buildPromtByUrl(string $url): void
    {
        $this->setPrompt(
            $this->promtBuilder
                ->setSubject('список ключевых слов')
                ->setFormat('json')
                ->setSource('url', $url)
                ->build()
        );
    }
}