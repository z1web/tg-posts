<?php

namespace App\Service\OpenAi\Requests;

use App\Service\OpenAi\OpenAiRequest;
use App\Service\OpenAi\ResponseHandlers\OpenAiHtmlBodyResponseHandler;

class OpenAiGenerateAppendArticleRequest extends OpenAiRequest
{
    public function __construct()
    {
        parent::__construct();
        $this->handler->addHandler(new OpenAiHtmlBodyResponseHandler());
    }

    public function setUrl(string $url): void
    {
        $this->setPrompt(
            "Допиши абзац статьи в формате html для страницы: {$url}, тему пойми исходя из контента страницы"
        );
    }
}
