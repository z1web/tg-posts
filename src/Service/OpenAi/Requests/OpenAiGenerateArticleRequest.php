<?php

namespace App\Service\OpenAi\Requests;

use App\Service\OpenAi\OpenAiRequest;
use App\Service\OpenAi\ResponseHandlers\OpenAiHtmlBodyResponseHandler;

/**
 * TODO: Generate article by tags!)
 * TODO: Parse image !
 */

class OpenAiGenerateArticleRequest extends OpenAiRequest
{
    private string $subject = 'статью';
    private string $format = 'html';
    private string $sourceType = 'theme';

    public function __construct()
    {
        parent::__construct();
        $this->handler->addHandler(new OpenAiHtmlBodyResponseHandler());
    }

    public function buildPromt(string $articleTheme): void
    {
        $promt = $this->promtBuilder
            ->setSubject($this->subject)
            ->setFormat($this->format)
            ->setSource($this->sourceType, $articleTheme)
            ->build();

        $this->setPrompt($promt);
    }
}
