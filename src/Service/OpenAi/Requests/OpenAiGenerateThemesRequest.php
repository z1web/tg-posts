<?php

namespace App\Service\OpenAi\Requests;

use App\Service\OpenAi\OpenAiRequest;
use App\Service\OpenAi\ResponseHandlers\OpenAiJsonToArrayResponseHandler;

//Категория -> Тема -> Вопрос -> Абзацы

class OpenAiGenerateThemesRequest extends OpenAiRequest
{
    private string $lang = 'ru';
    private array $subject = [
        'ru' => 'список трех тем (ключ - значение) для статьи',
        'en' => 'themes list'
    ];

    public function __construct()
    {
        parent::__construct();
        $this->handler->addHandler(new OpenAiJsonToArrayResponseHandler());
    }

    public function buildPromt(string $category): void
    {
        $this->setPrompt(
            $this->promtBuilder
                ->setSubject($this->subject[$this->lang])
                ->setFormat('json')
                ->setSource('theme', $category)
                ->build()
        );
    }
}