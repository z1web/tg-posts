<?php

namespace App\Service\OpenAi;

final class OpenAiRequestExecutor
{
    protected OpenAiClient $openAiClient;

    public function __construct(OpenAiClient $openAiClient)
    {
        $this->openAiClient = $openAiClient;
    }

    public function execute(OpenAiRequest $openAiRequest): mixed
    {
        $response = $this->openAiClient->request($openAiRequest->getObject(), $openAiRequest->getPrompt());
        return $openAiRequest->handler->handleResponse($response);
    }
}