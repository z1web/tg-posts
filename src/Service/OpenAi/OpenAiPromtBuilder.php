<?php

namespace App\Service\OpenAi;

class OpenAiPromtBuilder
{
    private string $subject;
    private string $format;
    private string $sourceType;
    private string $sourceValue;

    public function setSubject(string $subject): self
    {
        $this->subject = $subject;
        return $this;
    }

    public function setFormat(string $format): self
    {
        $this->format = $format;
        return $this;
    }

    public function setSource(string $sourceType, string $sourceValue): self
    {
        $this->sourceType = $sourceType;
        $this->sourceValue = $sourceValue;
        return $this;
    }

    public function build(): string
    {
        $sourceString = ', ';
        if ($this->sourceType === 'theme') {
            $sourceString = "по теме: {$this->sourceValue}";
        } elseif ($this->sourceType === 'url') {
            $sourceString = "для страницы: {$this->sourceValue}";
        }

        return "Составь {$this->subject} в формате {$this->format} {$sourceString}";
    }

}