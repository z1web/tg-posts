<?php

namespace App\Service\OpenAi;

use Symfony\Component\HttpClient\Exception\TransportException;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class OpenAiClient
{
    protected HttpClientInterface $httpClient;
    protected string $url = 'https://api.openai.com/v1/chat/';
    protected string $apiKey;

    public function __construct(string $apiKey)
    {
        $this->apiKey = $apiKey;
        $this->httpClient = HttpClient::create();
    }

    public function request(string $object, string $content): string
    {
        try {
            $response = $this->httpClient->request('POST', $this->url . $object, [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => "Bearer {$this->apiKey}",
                ],
                'timeout' => 150,
                'body' => '{
                     "model": "gpt-3.5-turbo",
                     "messages": [{"role": "user", "content": "' . $content . '"}],
                     "temperature": 0.7
                   }'
            ]);
        } catch (ClientExceptionInterface|TransportExceptionInterface $e) {
            throw new \RuntimeException($e->getMessage());
        }

        try {
            $result = $response->toArray();
        } catch (TransportExceptionInterface
        | ClientExceptionInterface
        | DecodingExceptionInterface
        | RedirectionExceptionInterface
        | ServerExceptionInterface $e) {
            throw new TransportException($e->getMessage());
        }

        if (!isset($result['choices'][0]['message']['content'])) {
            throw new \RuntimeException('OpenAi Response error');
        }

        return $result['choices'][0]['message']['content'];
    }
}