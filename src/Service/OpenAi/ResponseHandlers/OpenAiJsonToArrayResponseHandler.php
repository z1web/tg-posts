<?php

namespace App\Service\OpenAi\ResponseHandlers;

use App\Service\OpenAi\OpenAiResponseHandlerInterface;
use JsonException;

class OpenAiJsonToArrayResponseHandler implements OpenAiResponseHandlerInterface
{
    /**
     * @throws JsonException
     */
    public function handleResponse(mixed $response): array
    {
        try {
            $array = json_decode($response, 1, 512, JSON_THROW_ON_ERROR);
        } catch (JsonException $e) {
            throw new JsonException($e->getMessage());
        }

        $result = [];
        foreach ($array as $item) {
            if (is_array($item)) {
                foreach ($item as $value) {
                    $result[] = $value;
                }
            } elseif (is_string($item)) {
                $result[] = $item;
            }
        }

        return $result;
    }
}