<?php
namespace App\Service\OpenAi\ResponseHandlers;

use App\Service\OpenAi\OpenAiResponseHandlerInterface;
use Symfony\Component\DomCrawler\Crawler;

class OpenAiHtmlBodyResponseHandler implements OpenAiResponseHandlerInterface
{
    public function handleResponse(mixed $response): string
    {
        $crawler = new Crawler();
        $crawler->addHtmlContent($response);
        $html = $crawler->filter('body')->first()->html();

        if (!$html) {
            $html = $response;
        }

        return $this->removeSpecialChars($html);
    }

    public function removeSpecialChars(string $string): string
    {
        return str_replace(array("\r\n", "\r", "\n", "\t"), '', $string);
    }
}