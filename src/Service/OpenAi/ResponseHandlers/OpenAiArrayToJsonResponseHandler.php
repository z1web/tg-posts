<?php

namespace App\Service\OpenAi\ResponseHandlers;

use App\Service\OpenAi\OpenAiResponseHandlerInterface;
use JsonException;

class OpenAiArrayToJsonResponseHandler implements OpenAiResponseHandlerInterface
{
    /**
     * @throws JsonException
     */
    public function handleResponse(mixed $response): bool|string
    {
        try {
            $json = json_encode($response, JSON_THROW_ON_ERROR);
        } catch (JsonException $e) {
            throw new JsonException($e->getMessage());
        }

        return $json;
    }
}