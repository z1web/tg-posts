<?php

namespace App\Service\OpenAi;
/**
 * Pattern - Chain of Responsibility
 */
class OpenAiResponseHandlerContainer implements OpenAiResponseHandlerInterface
{
    protected array $handlers = [];

    public function handleResponse(mixed $response): mixed
    {
        $result = null;
        foreach ($this->handlers as $handler) {
            /**
             * @var $handler OpenAiResponseHandlerInterface
             */
            if ($result) {
                $response = $result;
            }
            $result = $handler->handleResponse($response);
        }

        return $result;
    }

    public function addHandler(OpenAiResponseHandlerInterface $handler): void
    {
        $this->handlers[] = $handler;
    }
}