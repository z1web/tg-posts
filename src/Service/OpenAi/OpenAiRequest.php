<?php

namespace App\Service\OpenAi;

use JetBrains\PhpStorm\Pure;

class OpenAiRequest
{
    private string $prompt;
    private string $object = 'completions';
    public OpenAiResponseHandlerInterface $handler;
    protected OpenAiPromtBuilder $promtBuilder;

    #[Pure]
    public function __construct()
    {
        $this->handler = new OpenAiResponseHandlerContainer();
        $this->promtBuilder = new OpenAiPromtBuilder();
    }

    public function setObject(string $object): void
    {
        $this->object = $object;
    }

    public function getObject(): string
    {
        return $this->object;
    }

    public function setPrompt(string $prompt): void
    {
        $this->prompt = $prompt;
    }

    public function getPrompt(): string
    {
        return $this->prompt;
    }
}