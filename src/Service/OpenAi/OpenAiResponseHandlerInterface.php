<?php

namespace App\Service\OpenAi;

interface OpenAiResponseHandlerInterface
{
    public function handleResponse(mixed $response): mixed;
}