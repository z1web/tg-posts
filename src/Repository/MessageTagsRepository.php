<?php

namespace App\Repository;

use App\Entity\MessageTags;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MessageTags|null find($id, $lockMode = null, $lockVersion = null)
 * @method MessageTags|null findOneBy(array $criteria, array $orderBy = null)
 * @method MessageTags[]    findAll()
 * @method MessageTags[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MessageTagsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MessageTags::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(MessageTags $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(MessageTags $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }


    public function getByMessageId(int $id, bool $associativeArrayResult = false): array {
        $messageTags = $this->_em->getRepository(MessageTags::class)
            ->findBy(['message_id' => $id]);

        if (!$associativeArrayResult) {
            return $messageTags;
        }

        $result = [];
        foreach ($messageTags as $messageTag) {
            if (!$messageTag->getParentId()) {
                $result[$messageTag->getId()]['value'] = $messageTag->getValue();
            }
        }
        foreach ($messageTags as $messageTag) {
            if ($messageTag->getParentId()) {
                $result[$messageTag->getParentId()]['sub_tags'][] = $messageTag->getValue();
            }
        }

        return $result;
    }

    // /**
    //  * @return MessageTags[] Returns an array of MessageTags objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MessageTags
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
