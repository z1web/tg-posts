<?php

namespace App\Controller;

use App\Entity\ChannelMessages;
use App\Entity\MessageTags;
use App\Repository\ChannelMessagesRepository;
use App\Repository\MessageTagsRepository;
use App\Service\TextParserService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Exception;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    #[Route('/', name: 'index')]
    public function index(
        EntityManagerInterface $entityManager, PaginatorInterface $paginator, Request $request): Response
    {
        $channels = $entityManager->createQueryBuilder()
            ->select('cm.channel_name')
            ->distinct()
            ->from(ChannelMessages::class, 'cm')
            ->getQuery()->execute();

        $messages = $entityManager->getRepository(ChannelMessages::class)->findBy(array(), array('date' => 'DESC'));
        $pagination = $paginator->paginate($messages, $request->query->getInt('page', 1), 12);

        return $this->render(
            'index-page.html.twig',
            ['channels' => $channels, 'pagination' => $pagination]
        );
    }

    #[Route('/channel/{channel}')]
    public function showChannel(EntityManagerInterface $entityManager, Request $request): Response
    {
        $messages = $entityManager
            ->getRepository(ChannelMessages::class)
            ->findBy(
                ['channel_name' => $request->get('channel')],
                array('date' => 'DESC')
            );


        return $this->render('show-channel.html.twig', [
            'messages' => $messages,
            'channelName' => $request->get('channel')
        ]);
    }


    #[Route('/channel/{channel}/{slug}')]
    public function showMessage(
        EntityManagerInterface $entityManager,
        Request $request,
        MessageTagsRepository $messageTagsRepository
    ): Response
    {
        /**
         * @var $message ChannelMessages
         */
        $message = $entityManager->getRepository(ChannelMessages::class)
            ->findOneBy([
                'slug' => $request->get('slug'),
                'channel_name' => $request->get('channel')
            ]);
        if (!$message) {
            throw $this->createNotFoundException('The article does not exist');
        }
        $messageTags = $messageTagsRepository->getByMessageId($message->getId(), true);

        $metaKeys = '';
        $i = 0;
        foreach ($messageTags as $key => $messageTag) {
            $i++;
            $metaKeys .= $messageTag['value'];
            if ($i < count($messageTags)) {
                $metaKeys .= ',';
            }
        }

        $messageName = (new TextParserService())->parseHeaderFromText($message->getMessageBody());

        return $this->render('show-message.html.twig', [
            'message' => $message,
            'messageName' => $messageName,
            'messageTags' => $messageTags,
            'metaKeys' => $metaKeys
        ]);
    }
}